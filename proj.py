import numpy as np
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

train_X1 = np.array([[8.0898, 8.0898, 8.0898, 90, 90, 90], [8.0889, 8.0889, 8.0889, 90, 90, 90], [8.0871, 8.0871, 8.0871, 90, 90, 90], [6.6077, 6.6077, 5.9957, 90, 90, 90], [6.6049, 6.6049, 5.9801, 90, 90, 90], [6.6132, 6.6132, 6.0038, 90, 90, 90], [9.2122, 9.2122, 9.1893, 90, 90, 120], [9.2165, 9.2165, 9.2181, 90, 90, 120], [9.2177, 9.2177, 9.2241, 90, 90, 120], [4.6479, 8.7909, 8.3927, 90, 90, 90], [4.6490, 8.7942, 8.3896, 90, 90, 90], [4.6500, 8.7977, 8.3908, 90, 90, 90], [8.8882, 5.6198, 10.1565, 90, 115.440, 90], [8.8823, 5.6191, 10.1384, 90, 115.380, 90], [8.9011, 5.6460, 10.1672, 90, 115.321, 90], [8.1821, 12.8752, 14.1772, 93.211, 115.841, 91.201], [8.1749, 12.8592, 7.1125, 93.521, 116.246, 89.931], [8.1753, 12.8786, 7.1058, 93.420, 116.097, 90.416]])

train_y1 = np.array([1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 6, 6, 6])

train_X2 = np.array([[5.6417, 5.6417, 5.6417, 90, 90, 90], [5.9380, 5.9380, 5.9380, 90, 90, 90], [5.4639, 5.4639, 5.4639, 90, 90, 90], [4.5955, 4.5955, 2.9598, 90, 90, 90], [15.5331, 15.5331, 11.8147, 90, 90, 90], [4.4041, 4.4041, 2.8702, 90, 90, 90], [4.9134, 4.9134, 5.4042, 90, 90, 120], [4.9869, 4.9869, 17.0496, 90, 90, 120], [5.0329, 5.0329, 13.7432, 90, 90, 120], [7.4864, 7.6743, 5.7720, 90, 90, 90], [7.7931, 7.8963, 5.5550, 90, 90, 90], [4.9632, 7.9674, 5.7442, 90, 90, 90], [5.2011, 9.0222, 20.0431, 90, 95.781, 90], [9.7441, 8.8954, 5.2725, 90, 106.099, 90], [5.0095, 5.8455, 10.3441, 90, 92.420, 90], [7.1240, 7.8517, 5.5762, 89.861, 101.131, 106.032], [8.5715, 12.9645, 7.2203, 90.741, 115.944, 87.664], [8.1351, 12.7842, 7.1594, 94.272, 116.594, 87.718]])

train_y2 = np.array([1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 6, 6, 6])

test_X1 = np.array([[8.0911, 8.0911, 8.0911, 90, 90, 90], [8.2132, 8.2132, 8.2132, 90, 90, 90], [6.6058, 6.6058, 5.9819, 90, 90, 90], [6.6042, 6.6042, 5.9722, 90, 90, 90], [9.2129, 9.2129, 9.1981, 90, 90, 120], [9.2209, 9.2209, 9.2215, 90, 90, 120], [4.6488, 8.7952, 8.3913, 90, 90, 90], [4.6509, 8.7996, 8.3902, 90, 90, 90], [8.8892, 5.6296, 10.1554, 90, 115.410, 90], [8.9013, 5.6374, 10.1644, 90, 115.407, 90], [8.1809, 12.8811, 7.1101, 93.466, 116.111, 90.370], [8.1792, 12.8799, 7.1032, 93.451, 116.035, 90.512]])

test_y1 = np.array([1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6])

test_X2 = np.array([[3.5504, 3.5504, 3.5504, 90, 90, 90], [5.4175, 5.4175, 5.4175, 90, 90, 90], [4.1613, 4.1613, 2.6631, 90, 90, 90], [4.7374, 4.7374, 3.1864, 90, 90, 90], [4.7602, 4.7602, 12.9946, 90, 90, 120], [15.9866, 15.9866, 7.2813, 90, 90, 120], [18.2567, 8.8467, 5.1918, 90, 90, 90], [6.8693, 8.3583, 5.3510, 90, 90, 90], [9.4805, 11.9372, 3.2431, 90, 98.762, 90], [8.5603, 13.0248, 7.1868, 90, 115.991, 90], [7.5035, 11.6694, 6.6991, 91.466, 93.923, 104.210], [10.1372, 11.0921, 7.3250, 99.513, 100.614, 83.365]])

test_y2 = np.array([1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6])

def NNeighbor (n):
    clf = KNeighborsClassifier(n)
    clf.fit(train_X1, train_y1)
    print('Training on Training 1')
    print('Testing 1 Prediction: ', clf.predict(test_X1), ' ', 1-((clf.predict(test_X1) == test_y1).sum()/len(test_y1)), ' error')
    print('Testing 2 Prediction: ', clf.predict(test_X2), ' ', 1-((clf.predict(test_X2) == test_y2).sum()/len(test_y2)), ' error')
    print('\n')
    clf2 = KNeighborsClassifier(n)
    clf2.fit(train_X2, train_y2)
    print('Training on Training 2')
    print('Testing 1 Prediction: ', clf2.predict(test_X1), ' ', 1-((clf2.predict(test_X1) == test_y1).sum()/len(test_y1)), ' error')
    print('Testing 2 Prediction: ', clf2.predict(test_X2), ' ', 1-((clf2.predict(test_X2) == test_y2).sum()/len(test_y2)), ' error')
    
def MNNB ():
    clf = MultinomialNB(alpha=1e-10)
    clf.fit(train_X1, train_y1)
    print('Training on Training 1')
    print('Testing 1 Prediction: ', clf.predict(test_X1), ' ', 1-((clf.predict(test_X1) == test_y1).sum()/len(test_y1)), ' error')
    print('Testing 2 Prediction: ', clf.predict(test_X2), ' ', 1-((clf.predict(test_X2) == test_y2).sum()/len(test_y2)), ' error')
    print('\n')
    clf2 = MultinomialNB(alpha=1e-10)
    clf2.fit(train_X2, train_y2)
    print('Training on Training 2')
    print('Testing 1 Prediction: ', clf2.predict(test_X1), ' ', 1-((clf2.predict(test_X1) == test_y1).sum()/len(test_y1)), ' error')
    print('Testing 2 Prediction: ', clf2.predict(test_X2), ' ', 1-((clf2.predict(test_X2) == test_y2).sum()/len(test_y2)), ' error')
    
def GNB ():
    clf = GaussianNB()
    clf.fit(train_X1, train_y1)
    print('Training on Training 1')
    print('Testing 1 Prediction: ', clf.predict(test_X1), ' ', 1-((clf.predict(test_X1) == test_y1).sum()/len(test_y1)), ' error')
    print('Testing 2 Prediction: ', clf.predict(test_X2), ' ', 1-((clf.predict(test_X2) == test_y2).sum()/len(test_y2)), ' error')
    print('\n')
    clf2 = GaussianNB()
    clf2.fit(train_X2, train_y2)
    print('Training on Training 2')
    print('Testing 1 Prediction: ', clf2.predict(test_X1), ' ', 1-((clf2.predict(test_X1) == test_y1).sum()/len(test_y1)), ' error')
    print('Testing 2 Prediction: ', clf2.predict(test_X2), ' ', 1-((clf2.predict(test_X2) == test_y2).sum()/len(test_y2)), ' error')

def SVCf ():
    clf = SVC(kernel='linear')
    clf.fit(train_X1, train_y1)
    print('Training on Training 1')
    print('Testing 1 Prediction: ', clf.predict(test_X1), ' ', 1-((clf.predict(test_X1) == test_y1).sum()/len(test_y1)), ' error')
    print('Testing 2 Prediction: ', clf.predict(test_X2), ' ', 1-((clf.predict(test_X2) == test_y2).sum()/len(test_y2)), ' error')
    print('\n')
    clf2 = SVC(kernel='linear')
    clf2.fit(train_X2, train_y2)
    print('Training on Training 2')
    print('Testing 1 Prediction: ', clf2.predict(test_X1), ' ', 1-((clf2.predict(test_X1) == test_y1).sum()/len(test_y1)), ' error')
    print('Testing 2 Prediction: ', clf2.predict(test_X2), ' ', 1-((clf2.predict(test_X2) == test_y2).sum()/len(test_y2)), ' error')